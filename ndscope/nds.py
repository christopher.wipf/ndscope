import os
import time
import collections
import numpy as np
from contextlib import closing
from PyQt4.QtCore import QThread, QMutex, pyqtSignal
import logging

import nds2

TREND_CTYPES = [
    nds2.channel.CHANNEL_TYPE_STREND,
    nds2.channel.CHANNEL_TYPE_MTREND,
]


HOSTPORT = os.getenv('NDSSERVER').split(',')[0].split(':')
HOST = HOSTPORT[0]
try:
    PORT = int(HOSTPORT[1])
except IndexError:
    PORT = 31200


def get_connection():
    logging.debug("NDS connect: {}:{}".format(HOST, PORT))
    conn = nds2.connection(HOST, PORT)
    conn.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_NAN')
    # conn.set_parameter('ITERATE_USE_GAP_HANDLERS', 'false')
    return conn


def iterate(channels, stride, start_end=None):
    with closing(get_connection()) as conn:
        args = []
        if start_end:
            args += list(start_end)
        if conn.get_protocol() == 1:
            stride = -1
        args += [stride]
        args += [channels]
        for bufs in conn.iterate(*args):
            yield bufs


def fetch(channels, start_end):
    args = list(start_end) + [channels]
    with closing(get_connection()) as conn:
        return conn.fetch(*args)


def parse_channel(channel):
    ctype = nds2.channel.channel_type_to_string(channel.channel_type)
    if ctype in ['s-trend', 'm-trend']:
        name, mod = channel.name.split('.')
    else:
        name = channel.name
        mod = 'raw'
    return name, mod, ctype

##################################################

class NDSThread(QThread):
    new_data = pyqtSignal('PyQt_PyObject')
    done = pyqtSignal('PyQt_PyObject')

    _type_map = {'raw': None, 'sec': 's', 'min': 'm'}

    def __init__(self, method, tid, trend, channels, start_end=None):
        assert method in ['fetch', 'iterate']
        if method == 'fetch':
            assert start_end is not None
        super(NDSThread, self).__init__()
        self.method = method
        self.tid = tid
        self.trend = trend
        self.channels = []
        for chan in channels:
            if trend == 'raw':
                self.channels.append(chan)
            else:
                for m in ['mean', 'min', 'max']:
                    t = self._type_map[self.trend]
                    self.channels.append('{}.{},{}-trend'.format(chan, m, t))
        if start_end:
            self.start_end = (int(start_end[0]), int(np.ceil(start_end[1])))
        else:
            self.start_end = None
        # for "fast" online data at 16Hz
        # FIXME: set explicitly
        self.stride = 1
        self._run_lock = QMutex()
        self._running = True

    def emit_data(self, bufs):
        self.new_data.emit((self.tid, self.trend, bufs))

    def emit_done(self, error=None):
        self.done.emit((self.tid, error))

    @property
    def running(self):
        try:
            self._run_lock.lock()
        # FIXME: python3
        #else:
            return self._running
        finally:
            self._run_lock.unlock()

    def run(self):
        logging.debug("NDS {} {} {}".format(self.method, self.start_end, self.channels))
        error = None

        if self.method == 'fetch':
            try:
                bufs = fetch(self.channels, self.start_end)
                self.emit_data(bufs)
                logging.debug("NDS fetched: {}".format(bufs[0].gps_seconds))
            except RuntimeError as e:
                error = str(e).split('\n')[0]

        elif self.method == 'iterate':
            try:
                for bufs in iterate(self.channels, self.stride, self.start_end):
                    if not self.running:
                        break
                    self.emit_data(bufs)
                    logging.debug("NDS iterate: {}".format(bufs[0].gps_seconds))
                    if not self.running:
                        break
            except RuntimeError as e:
                error = str(e).split('\n')[0]

        if error:
            logging.warning(error)
        self.emit_done(error)

    def stop(self):
        self._run_lock.lock()
        self._running = False
        self._run_lock.unlock()
