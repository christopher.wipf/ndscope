import collections
import numpy as np
from PyQt4.QtCore import QObject, pyqtSignal, pyqtSlot
import logging

from . import nds


SPAN_PADDING = 0.5


class DataBuffer(object):
    """data storage

    The data attribute here is actually a dict of sub-data arrays all
    with the same meta-data.  For trend data the keys should be
    ['mean', 'min', 'max'], and for full they would just be ['raw'].

    """

    __slots__ = ['channel', 'ctype', 'sample_rate',
                 'data', 'size', 'gps_start', 'lookback',
    ]

    def __init__(self, buf, lookback=None):
        """initialize with NDS Buffer object"""
        self.channel, mod, self.ctype = nds.parse_channel(buf.channel)
        self.sample_rate = buf.channel.sample_rate
        self.data = {}
        self.data[mod] = buf.data
        self.gps_start = buf.gps_seconds + buf.gps_nanoseconds*1e-9
        self.lookback = lookback or self.tlen
        
    def __repr__(self):
        return "<DataBuffer {}, start: {}, end: {}>".format(
            self.channel, self.gps_start, self.gps_end)

    def __len__(self):
        # FIXME: this is a hack way to doing this, and probably
        # doesn't perform well
        k = list(self.data.keys())[0]
        return self.data[k].size

    def is_trend(self):
        return self.ctype in ['s-trend', 'm-trend']

    def set_lookback(self, lookback):
        self.lookback = int(np.ceil(lookback))

    @property
    def step(self):
        return 1./self.sample_rate

    @property
    def tlen(self):
        """time length of buffer in seconds"""
        return len(self) * self.step

    @property
    def gps_end(self):
        return self.gps_start + self.tlen

    @property
    def tarray(self):
        return np.arange(len(self)) * self.step + self.gps_start

    def extend(self, buf):
        """extend buffer to right"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate
        assert buf.gps_start <= self.gps_end, "extend buffer start {} does not match end {}".format(buf.gps_start, self.gps_end)
        ind = np.where(buf.tarray > self.gps_end)[0]
        for mod in self.data:
            self.data[mod] = np.append(self.data[mod], buf.data[mod][ind])

    def extendleft(self, buf):
        """extend buffer to left"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate
        assert buf.gps_end == self.gps_start, "extendleft buffer end {} does not match start {}".format(buf.gps_end, self.gps_start)
        self.gps_start = buf.gps_start
        for mod in self.data:
            self.data[mod] = np.append(buf.data[mod], self.data[mod])

    def append(self, buf):
        """append data to the right, keeping overall time span"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate
        assert buf.gps_start == self.gps_end, "append buffer start {} does not match end {}".format(buf.gps_start, self.gps_end)
        # shift = data.size
        # nd = np.roll(self.data, -shift)
        # nd[-shift:] = data
        # trick for queue buffer array
        lbs = int(self.lookback * self.sample_rate)
        for mod in self.data:
            self.data[mod] = np.append(self.data[mod], buf.data[mod])[-lbs:]
        self.gps_start = max(
            self.gps_start,
            buf.gps_end - lbs*self.step,
        )


class DataBufferDict(object):
    """

    Takes NDS Buffer list at initialization and organizes the included
    data into a dictionary of DataBuffer objects keyd by channel.
    various trend channels are kept together in the same DataBuffer.

    """
    __slots__ = ['buffers', 'gps_start', 'gps_end', 'lookback']

    def __init__(self, nds_buffers, lookback):
        self.buffers = {}
        # nds buffer lists should have unique channel,ctype,mod
        # combos.
        for buf in nds_buffers:
            db = DataBuffer(buf, lookback)
            chan = db.channel
            if chan in self.buffers:
                for m, d in db.data.items():
                    self.buffers[chan].data[m] = d
            else:
                self.buffers[chan] = db
        self.gps_start = db.gps_start
        self.gps_end = db.gps_end
        self.lookback = lookback

    def __repr__(self):
        return "<DataBufferDict {}>".format(
            list(self.buffers.values()))

    def __getitem__(self, channel):
        return self.buffers[channel]

    def items(self):
        return self.buffers.items()

    def values(self):
        return self.buffers.values()

    def set_lookback(self, lookback):
        for db in self.buffers.values():
            db.set_lookback(lookback)

    def append(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].append(buf)
        self.gps_start = self.buffers[chan].gps_start
        self.gps_end = self.buffers[chan].gps_end

    def extendleft(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].extendleft(buf)
        self.gps_start = self.buffers[chan].gps_start

    def extend(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].extend(buf)
        self.gps_end = self.buffers[chan].gps_end


class DataStore(QObject):
    new_data = pyqtSignal('PyQt_PyObject')
    done = pyqtSignal('PyQt_PyObject')

    def __init__(self, status_widget, lookback=5):
        super(QObject, self).__init__()
        # use a counter to hold references to the channels, so that
        # channels as many channel referneces as needed can be added,
        # while only storing one set of channel data
        self._channels = collections.Counter()
        self.status = status_widget
        self._lookback = lookback
        self.nds = {}
        self.nds_last_error = None
        self.reset()

    def reset(self):
        logging.debug("DATA RESET")
        self.db = {k:None for k in ['raw', 'sec', 'min']}
        self.new_data.emit(None)

    def add_channel(self, channel):
        self._channels[channel] += 1
        assert self._channels[channel] >= 0
        print((channel, self._channels[channel]))

    def remove_channel(self, channel):
        self._channels[channel] -= 1
        assert self._channels[channel] >= 0
        print((channel, self._channels[channel]))

    @property
    def channels(self):
        # all channels with more than one referent
        return list(self._channels + collections.Counter())

    ##########

    @property
    def lookback(self):
        return self._lookback

    def set_lookback(self, lookback):
        self.db['raw'].set_lookback(lookback)
        self._lookback = lookback

    ##########

    def online(self, start_end=None):
        if self.nds:
            return
        self.nds_cmd('iterate', 'append', 'raw', start_end)

    def update(self, start_end, trend):
        # expand range to ints
        nstart = int(start_end[0])
        nend = int(np.ceil(start_end[1]))
        # padding
        pad = int((nend - nstart) * SPAN_PADDING)

        if self.nds:
            logging.debug("DATA BUSY")
            return

        if self.db[trend] is None:
            self.nds_cmd('fetch', 'full', trend, (nstart, nend))

        else:
            # emit what data we have, and will emit more later if it
            # turns out we need to extend the range
            self.new_data.emit(self.db[trend])
            # adjust the start/end times inward to make sure we
            # account for non-second aligned data due to 16Hz online
            start = int(np.ceil(self.db[trend].gps_start))
            end = int(self.db[trend].gps_end)
            if nstart < start:
                nstart -= pad
                self.nds_cmd('fetch', 'left', trend, (nstart, start))
            if nend > end:
                nend += pad
                self.nds_cmd('fetch', 'right', trend, (end, nend))

    ##########

    def nds_cmd(self, method, tid, trend, start_end=None):
        logging.debug("DATA NDS: {} {} {} {}".format(method, tid, trend, start_end))
        if trend == 'sec':
            ctype = "second trend"
        elif trend == 'min':
            ctype = "minute trend"
        elif tid == 'append':
            ctype = "online"
        else:
            ctype = "full"
        self.nds_last_error = None
        self.status.setStyleSheet("QStatusBar{background:rgba(0,100,0,100);}")
        self.status.showMessage("Retrieving {} data...".format(ctype))
        t = nds.NDSThread(method, tid, trend, self.channels, start_end)
        t.new_data.connect(self.nds_recv)
        t.done.connect(self.nds_done)
        t.start()
        self.nds[tid] = t

    @pyqtSlot('PyQt_PyObject')
    def nds_recv(self, recv):
        tid = recv[0]
        trend = recv[1]
        buffers = recv[2]

        # if tid not in self.nds:
        #     return

        logging.debug("DATA RECV {} {}".format(tid, trend))

        dbd = DataBufferDict(buffers, self._lookback)

        if tid == 'full':
            self.db[trend] = dbd
        elif tid == 'append':
            if not self.db[trend]:
                self.db[trend] = dbd
            else:
                self.db[trend].append(dbd)
        elif tid == 'left':
            self.db[trend].extendleft(dbd)
        elif tid == 'right':
            self.db[trend].extend(dbd)

        self.new_data.emit(self.db[trend])

    @pyqtSlot('PyQt_PyObject')
    def nds_done(self, recv):
        logging.debug("DATA DONE")
        tid, error = recv
        del self.nds[tid]
        if error:
            self.nds_last_error = error
            logging.warning("NDS error: {}".format(error))
            self.status.setStyleSheet("QStatusBar{background:rgba(255,0,0,255);color:black;font-weight:bold;}")
            self.status.showMessage("NDS error: {}".format(error))
        if not self.nds:
            self.notify_done()

    def terminate(self):
        logging.debug("DATA TERMINATE")
        for tid, nds in self.nds.items():
            # FIXME: thread terminate is causing problems on SL7
            #nds.terminate()
            nds.stop()
            nds = None
            self.nds[tid] = None
        self.notify_done()

    def notify_done(self):
        self.done.emit(self.nds_last_error)
        if not self.nds_last_error:
            self.status.setStyleSheet("")
            self.status.clearMessage()
