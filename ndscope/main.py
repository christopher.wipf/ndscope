# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import pyqtgraph as pg
from PyQt4 import QtGui
from PyQt4.QtGui import QStyle
from PyQt4.QtCore import pyqtSignal, pyqtSlot
import logging

from .designer import Ui_MainWindow
from .data import DataStore
from .trigger import Trigger

##################################################
# CONFIG

UPDATE_TIME = 50
# 4,915,200 points for 16kHz data
TREND_THRESHOLD_S = 300
#   259,200 points for 3*1Hz data
TREND_THRESHOLD_M = 86400

def set_background(color='k'):
    if color == 'w':
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

# pg.setConfigOption('leftButtonPan', False)
# see also ViewBox.setMouseMode(ViewBox.RectMode)
# file:///usr/share/doc/python-pyqtgraph-doc/html/graphicsItems/viewbox.html#pyqtgraph.ViewBox.setMouseMode

##################################################

class Scope(QtGui.QMainWindow, Ui_MainWindow):
    t0ChangeEvent = pyqtSignal()

    def __init__(self, channels, start_end=None):
        super(Scope, self).__init__(None)
        self.setupUi(self)

        ##########

        self.data = DataStore(self.statusBar)
        self.start_end = start_end
        self.online = False

        self.t0 = 0
        self.curves = {}
        self.colors = {}

        self.ttind = []
        self.trigger_time_set = False
        self.last_trigger = None

        ##########
        # create plots

        self.rows = 0
        self.cols = 0

        i = -1
        for r, row in enumerate(channels):
            self.rows += 1
            for c, sp in enumerate(row):
                i += 1
                self.cols += 1

                if sp:
                    plot = self.add_plot(r, c)

                for chan in sp:
                    self.add_channel_to_plot(plot, chan)

            # self.ttind.append(pg.InfiniteLine(angle=90, pen='r'))

        self.referenceTimeLabel = self._graphView.addLabel(
            "GPS Time",
            size='11pt',
            #bold=True,
            row=self.rows, col=0,
            colspan=self.cols+1)

        ##########
        # data retrieval

        self.data.new_data.connect(self.update_plots)
        self.data.done.connect(self.done)

        self._startstopButton.clicked.connect(self.startstop)
        self.fetchButton.clicked.connect(self.fetch)

        ##########
        # triggers

        self.trigger = None
        self.triggerTab.setEnabled(False)

        # self.trigger = Trigger(self._triggerGroup)
        # self._triggerSelect.addItems(self.channels)
        # self.triggerRegion = pg.LinearRegionItem(movable=False)
        # self.triggerRegion.setZValue(10)
        # self._triggerGroup.toggled.connect(self.toggle_trigger)
        # self._triggerSelect.currentIndexChanged.connect(self.update_trigger_channel)
        # self._triggerResetLevel.clicked.connect(self.reset_trigger_level)

        ##########
        # cursors

        self.cursorTTab.setEnabled(False)
        self.cursorYTab.setEnabled(False)

        # self.cursorYPlot.addItems(list(map(str, range(len(list(self.plot_iter()))))))
        # self.cursorYPlot.currentIndexChanged.connect(self.update_cursor_y_plot)

        # self.cursorT1.editingFinished.connect(lambda: self.update_cursor_value('T1'))
        # self.cursorT1.textChanged.connect(lambda: self.update_cursor_diff('T1'))
        # self.cursorT1Box.toggled.connect(lambda: self.toggle_cursor('T1'))
        # self.cursorT1Reset.clicked.connect(lambda: self.reset_cursor('T1'))

        # self.cursorT2.editingFinished.connect(lambda: self.update_cursor_value('T2'))
        # self.cursorT2.textChanged.connect(lambda: self.update_cursor_diff('T2'))
        # self.cursorT2Box.toggled.connect(lambda: self.toggle_cursor('T2'))
        # self.cursorT2Reset.clicked.connect(lambda: self.reset_cursor('T2'))

        # self.t0ChangeEvent.connect(self.update_t_cursor_t0)

        # self.cursorY1.editingFinished.connect(lambda: self.update_cursor_value('Y1'))
        # self.cursorY1.textChanged.connect(lambda: self.update_cursor_diff('Y1'))
        # self.cursorY1Box.toggled.connect(lambda: self.toggle_cursor('Y1'))
        # self.cursorY1Reset.clicked.connect(lambda: self.reset_cursor('Y1'))

        # self.cursorY2.editingFinished.connect(lambda: self.update_cursor_value('Y2'))
        # self.cursorY2.textChanged.connect(lambda: self.update_cursor_diff('Y2'))
        # self.cursorY2Box.toggled.connect(lambda: self.toggle_cursor('Y2'))
        # self.cursorY2Reset.clicked.connect(lambda: self.reset_cursor('Y2'))

        ##########
        # other widget setup

        self.offlineStartGPS.returnPressed.connect(self.fetch)
        self.offlineStartGPS.setValidator(QtGui.QIntValidator())
        self.offlineStopGPS.returnPressed.connect(self.fetch)
        self.offlineStopGPS.setValidator(QtGui.QIntValidator())

        ##########

        self.plot0.sigXRangeChanged.connect(self.update_range)
        self.setXRange(-self.data.lookback, 0)

    ##########

    def add_plot(self, row, col):
        plot = self._graphView.addPlot(
            # name=name,
            # title=title,
            row=row, col=col)

        # tie all plot x-axes together
        plot.setXLink(self.plot0)

        # Use automatic downsampling and clipping to reduce the
        # drawing load
        plot.setDownsampling(mode='peak')
        plot.setClipToView(True)
        # plot.disableAutoRange()
        # plot.enableAutoRange(y=True)
        # plot.setAutoVisible()
        # plot.hideButtons()
        plot.showGrid(x=True, y=True, alpha=0.2)
        plot.addLegend()

        # plot._cursor = {
        #     'T1': self.make_cursor('T1'),
        #     'T2': self.make_cursor('T2'),
        # }

        plot._channels = {}
        plot._bad_data = []
        plot._is_trend = False

        return plot

    @property
    def plot0(self):
        return self._graphView.getItem(0, 0)

    def plot_iter(self):
        for r in range(self.rows):
            for c in range(self.cols):
                plot = self._graphView.getItem(r, c)
                if plot is None:
                    continue
                yield plot

    def add_channel_to_plot(self, plot, channel):
        if channel in plot._channels:
            return

        if channel in self.colors:
            color = self.colors[channel]
        else:
            color = tuple(np.random.rand(3)*255)
            self.colors[channel] = color
        mmc = color+(50,)

        cs = {}
        cs['y'] = plot.plot(pen=color, name=channel)
        cs['min'] = pg.PlotDataItem(pen=mmc)
        cs['max'] = pg.PlotDataItem(pen=mmc)
        # FIXME: HACK for bug in old pyqtgraph 0.9
        try:
            cs['fill'] = pg.FillBetweenItem(cs['min'], cs['max'], brush=mmc)
        except:
            cs['fill'] = None
        plot._channels[channel] = cs

        self.data.add_channel(channel)

    def remove_channel_from_plot(self, plot, channel):
        if channel not in plot._channels:
            return
        for curve in plot._curves[channel].values():
            plot.removeItem(curve)
        del plot._channels[channel]
        self.data.remove_channel(channel)

    ##########

    def start(self):
        """Start online mode

        """
        logging.debug('START')
        self.online = True
        self._startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self._startstopButton.setText("Stop")
        self.offlineTab.setEnabled(False)
        self.clear_trigger_time()
        self.toggle_trigger()
        self.data.reset()
        self.data.online(start_end=self.start_end)
        self.setXRange(-self.data.lookback, 0)

    @pyqtSlot()
    def done(self):
        self._startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self._startstopButton.setText("Online")
        self.offlineTab.setEnabled(True)
        self.online = False

    def stop(self, message=None):
        """Stop online mode

        """
        logging.debug('STOP')
        self.data.terminate()

    def startstop(self):
        """Toggle online mode

        """
        if self.online:
            self.stop()
        else:
            self.start()

    def fetch(self, start_end=None):
        """Fetch data offline

        """
        if not start_end:
            start = int(self.offlineStartGPS.text())
            end = int(self.offlineStopGPS.text())
        else:
            start, end = start_end
        logging.debug('FETCH {}'.format((start, end)))
        self.toggle_trigger(False)
        self.data.reset()
        self.set_t0(end)
        self.setXRange(start-self.t0, end-self.t0)
        self.data_update(start, end)
        self.updateGPS()

    def update_range(self, *args):
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        if self.online:
            lookback = np.abs(xmin)
            logging.debug('update lookback: {}'.format(lookback))
            self.data.set_lookback(lookback)
        elif self._updateOnRange.isChecked():
            start = self.t0 + xmin
            end = self.t0 + xmax
            logging.debug('update range: {}'.format((start, end)))
            self.data_update(start, end)
        self.updateGPS()

    ##########

    def data_update(self, start, end):
        span = end - start
        if span > TREND_THRESHOLD_M:
            trend = 'min'
            start -= start % 60
            end -= (end % 60) + 60
        elif span > TREND_THRESHOLD_S:
            trend = 'sec'
        else:
            trend = 'raw'
        self.data.update((start, end), trend)

    @pyqtSlot('PyQt_PyObject')
    def update_plots(self, data):
        logging.debug("UPDATE PLOT")

        if not data:
            self.clear_plots()
            return

        # trigger = False
        # if self.online and self.trigger_active():
        #     trigger = self.check_trigger(data)
        #     if not trigger:
        #         return
        #     self.update_trigger_time()

        if self.online:
            self.set_t0(data.gps_end)

        for plot in self.plot_iter():
            for chan, curve in plot._channels.items():
                cd = data[chan]
                t = cd.tarray - self.t0
                if cd.is_trend():
                    y = cd.data['mean']
                    curve['min'].setData(x=t, y=cd.data['min'], connect="finite")
                    curve['max'].setData(x=t, y=cd.data['max'], connect="finite")
                    if not plot._is_trend:
                        plot.addItem(curve['min'])
                        plot.addItem(curve['max'])
                        if curve['fill']:
                            plot.addItem(curve['fill'])
                    plot._is_trend = True
                else:
                    y = cd.data['raw']
                    if plot._is_trend:
                        plot.removeItem(curve['min'])
                        plot.removeItem(curve['max'])
                        if curve['fill']:
                            plot.removeItem(curve['fill'])
                    plot._is_trend = False

                curve['y'].setData(x=t, y=y, connect="finite")
                if np.any(np.isnan(y)):
                    # FIXME: this does not work for discontinous
                    # regions
                    nani = np.where(np.isnan(y))[0]
                    r = (t[nani.min()], t[nani.max()])
                    if r not in plot._bad_data:
                        plot.addItem(pg.LinearRegionItem(
                            values=r,
                            #orientation='vertical',
                            brush=(250, 0, 0, 30),
                            #pen=None,
                            movable=False,
                        ))
                        plot._bad_data.append(r)

        # if trigger and self._triggerSingle.isChecked():
        #     logging.debug("trigger single shot")
        #     self.stop()

    def clear_plots(self):
        for plot in self.plot_iter():
            for curves in plot._channels.values():
                curves['y'].setData(np.array([0, 0]))
                curves['min'].setData(np.array([0, 0]))
                curves['max'].setData(np.array([0, 0]))

    def set_t0(self, t0):
        self.t0 = t0
        self.referenceTimeLabel.setText('{:0.4f}'.format(self.t0))
        self.updateGPS()
        # FIXME: just use change event on above
        self.t0ChangeEvent.emit()

    def setXRange(self, start, end):
        logging.debug('RANGE: {} {}'.format(start, end))
        self.plot0.sigXRangeChanged.disconnect()
        self.plot0.setXRange(start, end, padding=0, update=False)
        self.plot0.sigXRangeChanged.connect(self.update_range)

    def updateGPS(self):
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        start = int(self.t0 + xmin)
        end = int(np.ceil(self.t0 + xmax))
        self.offlineStartGPS.setText(str(start))
        self.offlineStopGPS.setText(str(end))

    ##########

    def trigger_active(self):
        return self._triggerGroup.isChecked()

    def set_trigger_channel(self, chan):
        plot = self.plots[self.cpmap[str(chan)]]
        plot.addItem(self.trigger.level, ignoreBounds=True)
        plot.addItem(self.triggerRegion, ignoreBounds=True)
        plot.disableAutoRange(axis='y')
        self.triggerRegion.setRegion([-self.data[chan].stride, 0])

    def clear_trigger_channel(self, chan):
        plot = self.plots[self.cpmap[str(chan)]]
        plot.removeItem(self.trigger.level)
        plot.removeItem(self.triggerRegion)

    def update_trigger_channel(self, chan):
        last_chan = self.trigger.channel
        chan = str(self._triggerSelect.currentText())
        if chan == last_chan:
            return
        self.clear_trigger_channel(last_chan)
        self.trigger.channel = chan
        self.set_trigger_channel(chan)
        self.reset_trigger_level()
        logging.debug("trigger set: {}".format(chan))

    def update_trigger_time(self):
        if not self.trigger_time_set:
            for plot, ttind in zip(self.plots, self.ttind):
                plot.addItem(ttind)
            self.trigger_time_set = True
        if self.last_trigger:
            time = self.last_trigger - self.t0
            for ttind in self.ttind:
                ttind.setValue(time)

    def clear_trigger_time(self):
        for plot, ttind in zip(self.plot_iter(), self.ttind):
            plot.removeItem(ttind)
        self.trigger_time_set = False

    def reset_trigger_level(self):
        chan = self.trigger.channel
        if self.data[chan]:
            value = np.mean(self.data[chan].data)
        else:
            value = 0
        self.trigger.set_level(value)

    def set_trigger_level(self, value=None):
        if value:
            self.trigger.set_level(value)
        else:
            # only reset if no level has ever been set
            # FIXME: need better way to check this
            if not self.trigger._triggerLevel.text():
                self.reset_trigger_level()

    def toggle_trigger(self, checked=None):
        if not self.trigger:
            return

        if checked is None:
            checked = self._triggerGroup.isChecked()

        chan = self.trigger.channel

        # trigger turning on
        if checked:
            self.set_trigger_channel(chan)
            self.set_trigger_level()
            self.update_trigger_time()

        # trigger turning off
        else:
            self.clear_trigger_channel(chan)
            self.clear_trigger_time()

    def check_trigger(self, bufs):
        chan = self.trigger.channel
        buf = bufs[self.cpmap[chan]]
        y = buf.data
        t = buf.tarray - buf.gps_end
        region = self.triggerRegion.getRegion()
        ind = np.where((region[0] < t) & (t < region[1]))
        tind = self.trigger.check(y[ind])
        if tind is not None:
            time = t[ind][tind]
            gps = time + self.t0
            logging.debug('TRIGGER!: {}'.format(gps))
            self._triggerTime.setText('{:14.3f}'.format(gps))
            self.last_trigger = gps
            return True
        return False

    ##########

    def make_cursor(self, label):
        if label[0] == 'T':
            angle = 90
            lpos = 0.9
        elif label[0] == 'Y':
            angle = 0
            lpos = 0.02
        cur = pg.InfiniteLine(angle=angle, pen='g', label=label, movable=True)
        cur.label.setPosition(lpos)
        cur.sigPositionChanged.connect(lambda line: self.update_cursor_line(label, line))
        return cur

    @property
    def cursor_y_plot(self):
        yplot = int(self.cursorYPlot.currentText())
        return self.plots[yplot]

    def update_cursor_y_plot(self):
        ci = int(self.cursorYPlot.currentText())
        for i, plot in enumerate(self.plots):
            if i == ci and self.cursorY1Box.isChecked():
                if 'Y1' not in plot.cursor:
                    plot.cursor['Y1'] = self.make_cursor('Y1')
                    plot.addItem(plot.cursor['Y1'])
                self.reset_cursor('Y1')
            elif 'Y1' in plot.cursor:
                plot.removeItem(plot.cursor['Y1'])
                del plot.cursor['Y1']

            if i == ci and self.cursorY2Box.isChecked():
                if 'Y2' not in plot.cursor:
                    plot.cursor['Y2'] = self.make_cursor('Y2')
                    plot.addItem(plot.cursor['Y2'])
                self.reset_cursor('Y2')
            elif 'Y2' in plot.cursor:
                plot.removeItem(plot.cursor['Y2'])
                del plot.cursor['Y2']

    def update_cursor_value(self, label):
        value = float(getattr(self, 'cursor'+label).text())
        logging.debug("UPDATE LINE {}: {}".format(label, value))
        if label[0] == 'T':
            for plot in self.plots:
                plot.cursor[label].setValue(value-self.t0)
        elif label[0] == 'Y':
            self.cursor_y_plot.cursor[label].setValue(value)

    def update_cursor_line(self, label, line):
        value = line.value()
        logging.debug("UPDATE VALUE {}: {}".format(label, value))
        if label[0] == 'T':
            for plot in self.plots:
                plot.cursor[label].setValue(value)
            value += self.t0
        getattr(self, 'cursor'+label).setText('{:.6f}'.format(value))

    def update_t_cursor_t0(self):
        if self.cursorT1Box.isChecked():
            T1 = self.plot0.cursor['T1'].value() + self.t0
            self.cursorT1.setText('{:.6f}'.format(float(T1)))
        if self.cursorT2Box.isChecked():
            T2 = self.plot0.cursor['T2'].value() + self.t0
            self.cursorT2.setText('{:.6f}'.format(float(T2)))

    def update_cursor_diff(self, label):
        logging.debug("UPDATE DIFF {}".format(label))
        if label[0] == 'T':
            if self.cursorT1Box.isChecked() \
               and self.cursorT2Box.isChecked():
                diff = np.abs(float(self.cursorT1.text()) - float(self.cursorT2.text()))
                self.cursorTDiffSec.setText('{:.6f} s'.format(diff))
                self.cursorTDiffHz.setText('{:.6f} Hz'.format(1./diff))
                self.cursorTDiffSec.setEnabled(True)
                self.cursorTDiffHz.setEnabled(True)
        elif label[0] == 'Y':
            if self.cursorY1Box.isChecked() \
               and self.cursorY2Box.isChecked():
                diff = np.abs(float(self.cursorY1.text()) - float(self.cursorY2.text()))
                self.cursorYDiff.setText('{:.6f}'.format(diff))
                self.cursorYDiff.setEnabled(True)

    def reset_cursor(self, label):
        x, y = self.cursor_y_plot.viewRange()
        if label == 'T1':
            value = x[0] + np.diff(x)/3
        elif label == 'T2':
            value = x[0] + np.diff(x)*2/3
        if label == 'Y1':
            value = y[0] + np.diff(y)/3
        elif label == 'Y2':
            value = y[0] + np.diff(y)*2/3
        value = float(value)
        if label[0] == 'T':
            self.plot0.cursor[label].setValue(value)
        elif label[0] == 'Y':
            self.cursor_y_plot.cursor[label].setValue(value)

    def toggle_cursor(self, label):
        logging.debug("TOGGLE {}".format(label))
        if label[0] == 'T':
            if getattr(self, 'cursor'+label+'Box').isChecked():
                for plot in self.plots:
                    plot.addItem(plot.cursor[label])
                if getattr(self, 'cursor'+label).text() == '':
                    self.reset_cursor(label)
                self.update_cursor_diff(label)
            else:
                for plot in self.plots:
                    plot.removeItem(plot.cursor[label])
                self.cursorTDiffSec.setText('')
                self.cursorTDiffHz.setText('')
                self.cursorTDiffSec.setEnabled(False)
                self.cursorTDiffHz.setEnabled(False)

        elif label[0] == 'Y':
            self.update_cursor_y_plot()
            if getattr(self, 'cursor'+label+'Box').isChecked():
                self.update_cursor_diff(label)
            else:
                self.cursorYDiff.setText('')
                self.cursorYDiff.setEnabled(False)
