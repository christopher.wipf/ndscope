import pyqtgraph as pg
from PyQt4 import QtGui
import numpy as np
import logging


class Trigger:
    def __init__(self, widget_group):
        """
        """
        self.level = pg.InfiniteLine(angle=0, movable=True, label='trigger')
        self.level.label.setPosition(0.02)

        # initialize widgets
        for widget in widget_group.children():
            setattr(self, str(widget.objectName()), widget)
        self._triggerLevel.returnPressed.connect(self.update_level)
        self._triggerLevel.setValidator(QtGui.QDoubleValidator())
        self.level.sigPositionChangeFinished.connect(self.update_level)

        self.channel = None

    def get_level(self):
        return self.level.value()

    def set_level(self, value):
        self.level.setValue(value)
        self._triggerLevel.setText(str(value))

    def update_level(self, line=None):
        if line is not None:
            #val = line.value()
            value = self.get_level()
            self._triggerLevel.setText(str(value))
        else:
            value = float(self._triggerLevel.text())
            self.level.setValue(value)


    def check(self, y):
        """Check for trigger in data

        Returns time or None

        """
        level = self.level.value()
        yp = np.roll(y, 1)
        yp[0] = y[0]
        if self._triggerInvert.isChecked():
            tind = np.where((yp >= level) & (y < level))
        else:
            tind = np.where((yp <= level) & (y > level))
        if np.any(tind):
            return tind[0].min()
