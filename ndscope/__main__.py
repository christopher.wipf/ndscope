import sys
import argparse
import numpy as np
from PyQt4 import QtGui
import signal
try:
    from setproctitle import setproctitle
except ImportError:
    def setproctitle(*args):
        pass
import logging

from .main import set_background, Scope

logging.basicConfig(
    level='DEBUG',
    format="%(threadName)s:%(message)s",
)

##################################################

PROG = 'ndscope'
DESCRIPTION = '''
Next generation NDS time series plotter

Without time arguments immediately start plotting online data.  Left
and right mouse buttons control pan and zoom respectively, as does
center wheel.  In online mode, zoom out increases length of lookback
buffer.  In offline mode, zoom out and pan will fetch any missing data
to fill in range.  Second and minute trend data are substituted
automatically depending on the time scale being requested/displayed.

By default all channels are placed in a single plot.  Periods and
commas in the channel list (space separated) cause new subplots to be
created, with commas starting a new row of plots.  For instance,
specifying the channel list as:

  FOO . BAR BAZ , QUX

will cause three subplots to be created, two in the top row (the
second with the two channels BAR and BAZ), and one in the second.  The
'-l' option can be used to automatically lay out channels in a grid
(in row/column order).

please report issues: https://git.ligo.org/cds/ndscope/issues

'''

parser = argparse.ArgumentParser(
    prog=PROG,
    description=DESCRIPTION,
    formatter_class=argparse.RawDescriptionHelpFormatter,
)

parser.add_argument('channels', nargs='+',
                    help="channels")
parser.add_argument('-s', '--start', type=int,
                    help="start time")
parser.add_argument('-e', '--end', type=int,
                    help="end time")
parser.add_argument('-r', '--replay', action='store_true',
                    help="online replay data from (start, end)")
parser.add_argument('-bw', '--black-on-white', action='store_true',
                    help="black-on-white plots, instead of white-on-black")
parser.add_argument('-l', '--auto-layout', action='store_true',
                    help="arrange channels in a grid of plots")


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    setproctitle(PROG+' '+' '.join(sys.argv[1:]))

    args = parser.parse_args()

    if args.start and not args.end:
        args.end = args.start + 10
    elif args.end and not args.start:
        args.start = args.end - 10
    if args.start and args.end:
        start_end = (args.start, args.end)
    else:
        start_end = None

    if args.replay:
        if not start_end:
            sys.exit("Must specify start/end for replay.")
        replay = start_end
    else:
        replay = None

    r = 0
    c = 0
    if args.auto_layout:
        maxrows = 5.
        channels = [chan for chan in args.channels if chan not in ('.', ',')]

        num = len(channels)
        rows = int(np.floor(np.sqrt(num)))
        cols = int(np.ceil(float(num)/rows))

        chans = []
        for r in range(rows):
            chans.append([[]]*(cols))

        r = 0
        c = 0
        for i, chan in enumerate(channels):
            chans[r][c] = [chan]
            c += 1
            if c == cols:
                c = 0
                r += 1

    else:
        chans = [[[]]]
        for chan in args.channels:
            if chan == '.':
                chans[r].append([])
                c += 1
                continue
            if chan == ',':
                chans.append([])
                r += 1
                chans[r].append([])
                c = 0
                continue
            chans[r][c].append(chan)

    logging.debug("layout: {}".format(chans))

    if args.black_on_white:
        set_background('w')

    app = QtGui.QApplication([])
    scope = Scope(chans, start_end=replay)
    scope.show()

    if start_end and not replay:
        scope.fetch(start_end)
    else:
        scope.start()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
