PROJECT = ndscope
QTVERS = 4


$(PROJECT)/designer.py: %.py: %.ui
	pyuic$(QTVERS) $< -o $@

gui:
	designer $(PROJECT)/designer.ui
