from setuptools import setup


setup(
    name = 'ndscope',
    version = '0.1',
    description = "Next-generation NDS time series plotting",
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/cds/ndscope',
    license = 'GNU GPL v3+',

    # install_requires = [
    #     'nds2-client',
    #     'pyqtgraph',
    #     'pyqt4',
    #     'numpy',
    #     ],

    packages = ['ndscope'],
    
    entry_points={
        'console_scripts': [
            'ndscope = ndscope.__main__:main',
        ],
    },
)
