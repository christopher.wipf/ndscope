Next-generation NDS time series viewer
======================================

WARNING: This is a work in progress, so expect many things to change,
including the name.

Features:

* triggering of online data (oscilloscope mode)
* auto-fetch data when scrolling/re-scaling

Requirements

* python-nds2-client
* [pyqtgraph](http://pyqtgraph.org/)
* python-pyqt4
* pyqt4-dev-tools (dev)
* qt4-designer (dev)
